/*! \file  put9660.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 24, 2018, 1:59 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#ifndef FCY
#define FCY 80000000
#endif
#include <libpic30.h>
#include "../include/I2C.h"
#include "APDS9960.h"


/*! put9660 - */

/*!
 *
 */
void put9960(unsigned char ucRegister, unsigned char ucValue )
{
  unsigned char ucControlByte;
  ucControlByte = (ADPS9960_ADDRESS << 1)&0xfe; /* Read condition */

  I2Cstart(); /* Start I2C transaction            */
  I2Cwrite(ucControlByte); /* Address of DS1307 | write  */
  while (!I2CACKstatus())       // 3
    ;
  I2Cwrite(ucRegister);         // 4
  while (!I2CACKstatus())       // 5
    ;
  I2Cwrite(ucValue); /* Low 8 bits of value              */
  while (!I2CACKstatus())       // 5
    ;
  I2Cstop(); /* Stop the transaction             */
  __delay_ms(5); /* 5ms write cycle                  */

}
