/*! \file  APDS9960.h
 *
 *  \brief Constants and function prototypes for APDS9960
 *
 *  \author jjmcd
 *  \date November 29, 2018, 7:40 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef APDS9960_H
#define	APDS9960_H

#ifdef	__cplusplus
extern "C"
{
#endif
  
#define ADPS9960_ADDRESS 0x39

unsigned int get9960(unsigned char ucRegister);
void put9960(unsigned char ucRegister, unsigned char ucValue );



#ifdef	__cplusplus
}
#endif

#endif	/* APDS9960_H */

