/*! \file  get9960.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 23, 2018, 8:20 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/I2C.h"
#include "APDS9960.h"

/*! get9960 - */

/*!
 *
 */
unsigned int get9960(unsigned char ucRegister)
{
  unsigned char ucControlByte;
  unsigned int uResult;

  ucControlByte = (ADPS9960_ADDRESS << 1)&0xfe; /* Read condition */
  I2Cstart();                   // 1
  I2Cwrite(ucControlByte);      // 2
  while (!I2CACKstatus())       // 3
    ;
  I2Cwrite(ucRegister);         // 4
  while (!I2CACKstatus())       // 5
    ;
  I2Crestart();                 // 8
  I2Cwrite(ucControlByte|1 );     // 9
  while (!I2CACKstatus())       // 10
    ;
  uResult = I2Cget();           // 11
  I2CnotACK();                  // 12
  I2Cstop();                    // 13
  return uResult;
}
